libbpp-seq-omics (2.4.1-12) unstable; urgency=medium

  * Since every GCC version bump ends up in a bug caused by the need to
    update symbols files we stop providing symbols for this library
    Closes: #1075154
  * lintian-override for no-symbols-control-file
  * Standards-Version: 4.7.0

 -- Andreas Tille <tille@debian.org>  Wed, 07 Aug 2024 11:05:55 +0200

libbpp-seq-omics (2.4.1-11) unstable; urgency=medium

  * Team upload.
  * d/patches/unforce-cxxflags.patch: merge with upstream's flag,
    especially -std=c++11; forward our patch to upstream.
  * Silence some false-positive Lintian errors and overrides related to
    the t64 transition.
  * Add patch to fix a typo.

 -- Michael R. Crusoe <crusoe@debian.org>  Mon, 25 Mar 2024 19:02:40 +0100

libbpp-seq-omics (2.4.1-10.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062239

 -- Lukas Märdian <slyon@debian.org>  Thu, 29 Feb 2024 09:37:31 +0000

libbpp-seq-omics (2.4.1-10) unstable; urgency=medium

  * Team upload.
  * Update symbols file for GCC-13.
    Closes: #1037718, #1037721, #1037722, #1042137, #1042139, #1042204, #1042230
  * Standards-Version: 4.6.2 (routine-update)
  * d/control: use ${devlibs:Depends} for libbpp-seq-omics-dev

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 13 Aug 2023 12:22:12 +0200

libbpp-seq-omics (2.4.1-9) unstable; urgency=medium

  * Fix watch file

 -- Andreas Tille <tille@debian.org>  Mon, 28 Nov 2022 18:02:16 +0100

libbpp-seq-omics (2.4.1-8) unstable; urgency=medium

  * Adapt symbols to gcc-12
    Closes: #1012972
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Sun, 24 Jul 2022 16:42:49 +0200

libbpp-seq-omics (2.4.1-7) unstable; urgency=medium

  * Apply multi-arch hints.
    + libbpp-seq-omics-dev: Add Multi-Arch: same.
  * Update symbols file
    Closes: #984092

 -- Andreas Tille <tille@debian.org>  Tue, 12 Oct 2021 18:47:10 +0200

libbpp-seq-omics (2.4.1-6) unstable; urgency=medium

  * Fix watchfile to detect new versions on github
  * Standards-Version: 4.6.0 (routine-update)
  * Update symbols file
    Closes: #984092
  * Drop unneeded versioned Depends

 -- Andreas Tille <tille@debian.org>  Tue, 12 Oct 2021 12:08:43 +0200

libbpp-seq-omics (2.4.1-5) unstable; urgency=medium

  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Refer to specific version of license GPL-2+.
  * Update symbols for gcc-10
    Closes: #957431

 -- Andreas Tille <tille@debian.org>  Thu, 23 Jul 2020 12:19:17 +0200

libbpp-seq-omics (2.4.1-4) unstable; urgency=medium

  * Rebuild for new version of gcc to fix symbols
    Closes: #925737
  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * Trim trailing whitespace.

 -- Andreas Tille <tille@debian.org>  Tue, 20 Aug 2019 18:20:38 +0200

libbpp-seq-omics (2.4.1-3) unstable; urgency=medium

  * Rebuild for new version of gcc to fix symbols
    Closes: #924734

 -- Andreas Tille <tille@debian.org>  Sat, 16 Mar 2019 19:07:21 +0100

libbpp-seq-omics (2.4.1-2) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Use secure copyright file specification URI.
  * Trim trailing whitespace.

  [ Andreas Tille ]
  * Easier way to provide symbols for amd64 only
  * Adapt symbols
    Closes: #917591
  * Standards-Version: 4.3.0
  * Remove trailing whitespace in debian/copyright

 -- Andreas Tille <tille@debian.org>  Sun, 06 Jan 2019 17:30:24 +0100

libbpp-seq-omics (2.4.1-1) unstable; urgency=medium

  [ Julien Dutheil ]
  * New upstream version.
    Closes: #897785

  [ Andreas Tille ]
  * Standards-Version: 4.2.0

 -- Julien Dutheil <julien.dutheil@univ-montp2.fr>  Mon, 20 Aug 2018 08:48:12 +0200

libbpp-seq-omics (2.4.0-2) unstable; urgency=medium

  [ Julien Dutheil ]
  * Provide symbols file only for amd64
    Closes: #895212
  * Versioned Build-Depends: d-shlibs (>= 0.82)

  [ Andreas Tille ]
  * Provide full license text of CeCILL license
  * Point Vcs fields to salsa.debian.org

 -- Julien Dutheil <julien.dutheil@univ-montp2.fr>  Sun, 08 Apr 2018 18:29:27 +0200

libbpp-seq-omics (2.4.0-1) unstable; urgency=medium

  * New upstream version.

 -- Julien Dutheil <julien.dutheil@univ-montp2.fr>  Wed, 28 Mar 2018 10:00:23 +0200

libbpp-seq-omics (2.3.2-1) unstable; urgency=medium

  [ Julien Dutheil ]
  * Standards-Version: 4.1.2

  [ Andreas Tille ]
  * cme fix dpkg-control
  * debhelper 11

 -- Julien Dutheil <julien.dutheil@univ-montp2.fr>  Wed, 07 Feb 2018 10:45:02 +0200

libbpp-seq-omics (2.3.1-5) unstable; urgency=medium

  [ Julien Dutheil ]
  * Does not override debian cxx flags anymore.

  [ Andreas Tille ]
  * Rebuild using gcc-7
  * Standards-Version: 4.0.1

 -- Andreas Tille <tille@debian.org>  Thu, 24 Aug 2017 06:53:33 +0200

libbpp-seq-omics (2.3.1-4) unstable; urgency=medium

  * Fixed unit test CMake file for more portability.
  * Removed postinst scripts etc.

 -- Julien Dutheil <julien.dutheil@univ-montp2.fr>  Mon, 17 Jul 2017 22:56:32 +0200

libbpp-seq-omics (2.3.1-3) unstable; urgency=medium

  * cmake files belong to /usr/lib/$(DEB_HOST_MULTIARCH)
    Closes: #865901
  * d-shlibs (>= 0.80) contains needed override
  * Standards-Version: 4.0.0 (no changes needed)

 -- Andreas Tille <tille@debian.org>  Mon, 26 Jun 2017 10:10:20 +0200

libbpp-seq-omics (2.3.1-2) unstable; urgency=medium

  * Make sure cmake files will be installed also on i386

 -- Andreas Tille <tille@debian.org>  Thu, 22 Jun 2017 08:08:06 +0200

libbpp-seq-omics (2.3.1-1) unstable; urgency=medium

  * New upstream version
  * d/watch: point to github
  * debhelper 10
  * Adapt package names to soversion change
  * Hardening

 -- Andreas Tille <tille@debian.org>  Tue, 13 Jun 2017 23:24:40 +0200

libbpp-seq-omics (2.2.0-1) unstable; urgency=medium

  * New upstream version
  * Add watch file
  * Better description
  * Team maintenance in Debian Med team
  * cme fix dpkg-control
  * Source package in Section: science
  * debhelper 9
  * DEP5
  * short dh rules file using d-shlibs
  * Library transition
    Closes: #791100

 -- Andreas Tille <tille@debian.org>  Fri, 22 Apr 2016 21:26:04 +0200

libbpp-seq-omics (2.1.0-1) unstable; urgency=low

  * Maf to VCF tool added as a MafIterator.

 -- Julien Dutheil <julien.dutheil@univ-montp2.fr>  Wed, 06 Mar 2013 14:34:00 +0100

libbpp-seq-omics (2.0.3-1) unstable; urgency=low

  * Initial release, forking from bpp-seq.

 -- Julien Dutheil <julien.dutheil@univ-montp2.fr>  Tue, 06 Nov 2012 14:05:00 +0100
